/*
 Navicat Premium Data Transfer

 Source Server         : localdocker
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : novel

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 27/04/2022 13:02:37
*/
DELETE FROM sys_role_menu WHERE role_id='1' AND menu_id='1032';
DELETE FROM sys_menu WHERE id='1032';

INSERT INTO `sys_menu` VALUES (1076, '数据源监控', 1023, 5, '/monitor/datasource', 'C', '0', 'monitor:datasource:list', 'DataSource', NULL, '#', 'admin', '2022-04-19 14:19:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1077, 'SQL监控', 1023, 6, '/monitor/sql', 'C', '0', 'monitor:sql:list', 'Sql', NULL, '#', 'admin', '2022-04-20 14:04:07', 'admin', '2022-04-20 14:04:25', '');
INSERT INTO `sys_menu` VALUES (1078, 'WEB监控', 1023, 7, '/monitor/web', 'C', '0', 'monitor:web:list', 'Web', NULL, '#', 'admin', '2022-04-20 15:07:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1079, 'Url监控', 1023, 8, '/monitor/url', 'C', '0', 'monitor:url:list', 'Url', NULL, '#', 'admin', '2022-04-20 15:49:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1080, 'Spring监控', 1023, 9, '/monitor/spring', 'C', '0', 'monitor:spring:list', 'Spring', NULL, '#', 'admin', '2022-04-20 16:06:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1081, 'SQL防火墙', 1023, 10, '/monitor/wall', 'C', '0', 'monitor:wall:list', 'Wall', NULL, '#', 'admin', '2022-04-22 14:12:02', 'admin', '2022-04-22 14:14:16', '');
INSERT INTO `sys_role_menu` VALUES (1, 1076);
INSERT INTO `sys_role_menu` VALUES (1, 1077);
INSERT INTO `sys_role_menu` VALUES (1, 1078);
INSERT INTO `sys_role_menu` VALUES (1, 1079);
INSERT INTO `sys_role_menu` VALUES (1, 1080);
INSERT INTO `sys_role_menu` VALUES (1, 1081);