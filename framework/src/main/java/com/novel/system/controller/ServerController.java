package com.novel.system.controller;

import com.novel.common.cache.service.ICacheService;
import com.novel.framework.base.BaseController;
import com.novel.framework.result.Result;
import com.novel.framework.web.domain.Server;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author novel
 * @date 2019/5/21
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController extends BaseController {

    private final ICacheService cacheService;

    public ServerController(ICacheService cacheService) {
        this.cacheService = cacheService;
    }

    /**
     * 获取服务器运行信息
     *
     * @return 服务器运行信息
     */
    @RequiresPermissions("monitor:server:list")
    @GetMapping()
    public Result server() {
        Server server = new Server();
        server.copyTo();
        server.setCache(cacheService.getCacheInfo());
        return toAjax(server);
    }
}
