package com.novel.system.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.novel.framework.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


import java.io.Serializable;
import java.util.Date;

/**
 * 系统访问记录
 *
 * @author novel
 * @date 2019/12/20
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysLogininfor extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户账号
     */
    @Excel(name = "用户账号")
    private String userName;

    /**
     * 登录状态 0成功 1失败
     */
    @Excel(name = "登录状态", orderNum = "1", replace = {"成功_0", "失败_1"})
    private String status;

    /**
     * 登录IP地址
     */
    @Excel(name = "登录IP地址", orderNum = "2", width = 20)
    private String ipaddr;

    /**
     * 登录地点
     */
    @Excel(name = "登录地点", orderNum = "3", width = 20)
    private String loginLocation;

    /**
     * 浏览器类型
     */
    @Excel(name = "浏览器类型", orderNum = "4")
    private String browser;

    /**
     * 操作系统
     */
    @Excel(name = "操作系统", orderNum = "5", width = 15)
    private String os;

    /**
     * 提示消息
     */
    @Excel(name = "提示消息", orderNum = "6")
    private String msg;

    /**
     * 访问时间
     */
    @Excel(name = "访问时间", orderNum = "6", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    private Date loginTime;
}
