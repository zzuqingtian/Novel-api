package com.novel.system.domain;


import cn.afterturn.easypoi.excel.annotation.Excel;
import com.novel.framework.base.BaseModel;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 * 岗位
 *
 * @author novel
 * @date 2019/12/20
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public class SysPost extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 岗位编码
     */
    @NotBlank(message = "岗位编码不能为空", groups = {AddGroup.class})
    @Size(max = 10, message = "岗位编码长度不能超过10个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "岗位编码")
    private String postCode;

    /**
     * 岗位名称
     */
    @NotBlank(message = "岗位编码不能为空", groups = {AddGroup.class})
    @Size(max = 10, message = "岗位编码长度不能超过10个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "岗位名称")
    private String postName;

    /**
     * 岗位排序
     */
    @NotBlank(message = "岗位排序不能为空", groups = {AddGroup.class})
    @Range(min = 0, message = "岗位排序不正确", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "岗位排序")
    private String postSort;

    /**
     * 状态（0正常 1停用）
     */
    @NotBlank(message = "岗位状态不能为空", groups = {AddGroup.class})
    @Pattern(regexp = "^0|1$", message = "岗位状态错误", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "状态", replace = {"正常_0", "停用_1"})
    private String status;
}
