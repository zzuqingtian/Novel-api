package com.novel.framework.redis.cache;

import com.novel.framework.redis.ICacheService;
import com.novel.framework.shiro.config.JwtProperties;
import org.apache.shiro.cache.AbstractCacheManager;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;

/**
 * RedisCache  管理器
 *
 * @author novel
 * @date 2019/4/16
 */
public class ShiroRedisCacheManager extends AbstractCacheManager {

    private final ICacheService iCacheService;
    private final JwtProperties jwtProperties;

    public ShiroRedisCacheManager(ICacheService iCacheService, JwtProperties jwtProperties) {
        this.iCacheService = iCacheService;
        this.jwtProperties = jwtProperties;
    }

    /**
     * 为了个性化配置redis存储时的key，我们选择了加前缀的方式，所以写了一个带名字及redis操作的构造函数的Cache类
     *
     * @param name 缓存名称
     * @return Cache
     * @throws CacheException 异常
     */
    @Override
    protected Cache<String, Object> createCache(String name) throws CacheException {
        return new ShiroRedisCache<>(iCacheService, jwtProperties, name);
    }
}
