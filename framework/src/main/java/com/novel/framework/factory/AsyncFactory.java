package com.novel.framework.factory;

import com.novel.common.constants.Constants;
import com.novel.system.domain.LoginUser;
import com.novel.system.domain.SysLogininfor;
import com.novel.system.service.SysLogininforService;
import com.novel.system.service.SysUserService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 异步任务工厂
 *
 * @author novel
 * @date 2019/12/12
 */
@Component
@EnableAsync
public class AsyncFactory {
    private final SysUserService userService;
    private final SysLogininforService sysLogininforService;

    public AsyncFactory(SysUserService userService, SysLogininforService sysLogininforService) {
        this.userService = userService;
        this.sysLogininforService = sysLogininforService;
    }

    @Async
    public void recordLogininfor(LoginUser loginUser) {
        SysLogininfor logininfor = new SysLogininfor();
        logininfor.setBrowser(loginUser.getBrowser());
        logininfor.setIpaddr(loginUser.getIpaddr());
        logininfor.setLoginLocation(loginUser.getLoginLocation());
        logininfor.setLoginTime(new Date(loginUser.getLoginTime()));
        logininfor.setMsg("登录成功");
        logininfor.setStatus(Constants.SUCCESS);
        logininfor.setUserName(loginUser.getUser().getUserName());
        logininfor.setOs(loginUser.getOs());
        sysLogininforService.insertLogininfor(logininfor);
    }

    @Async
    public void updateLoginUser(LoginUser loginUser) {
        userService.updateUserLoginInfo(loginUser.getUser());
    }
}
