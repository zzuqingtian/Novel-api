package com.novel.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 演示模式异常
 *
 * @author novel
 * @date 2019/12/5
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DemoModeException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 1L;
    private String msg;

    public DemoModeException() {
    }

    public DemoModeException(String msg) {
        super(msg);
        setMsg(msg);
    }
}
