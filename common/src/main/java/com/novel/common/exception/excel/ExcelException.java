package com.novel.common.exception.excel;

import com.novel.common.exception.base.BaseException;


import java.io.Serializable;


/**
 * Execl 操作相关的异常类
 *
 * @author novel
 * @date 2018/12/14
 */
public class ExcelException extends BaseException  implements Serializable {

    private static final long serialVersionUID = 1L;

    public ExcelException(String module, String code, Object[] args, String defaultMessage) {
        super(module, code, args, defaultMessage);
    }

    public ExcelException(String module, String code, Object[] args) {
        super(module, code, args);
    }

    public ExcelException(String module, String defaultMessage) {
        super(module, defaultMessage);
    }

    public ExcelException(String code, Object[] args) {
        super(code, args);
    }

    public ExcelException(String defaultMessage) {
        super(defaultMessage);
    }
}
