package com.novel.common.exception.business;

import com.novel.common.exception.base.BaseException;


import java.io.Serializable;

/**
 * 全局业务异常
 *
 * @author novel
 * @date 2019/6/6
 */
public class BusinessException extends BaseException  implements Serializable {

    private static final long serialVersionUID = 1L;

    public BusinessException(String code, String message) {
        super(code, message);
    }

    public BusinessException(String s) {
        super(s);
    }
}
